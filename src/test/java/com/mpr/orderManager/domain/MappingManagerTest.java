package com.mpr.orderManager.domain;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.mpr.orderManager.service.MappingManager;

public class MappingManagerTest {
	MappingManager mManager = new MappingManager();
	
	@Test
	public void checkConnection(){
		assertNotNull(mManager.getConnection());
	}
}
