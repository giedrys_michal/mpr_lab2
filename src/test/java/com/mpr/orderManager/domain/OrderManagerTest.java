package com.mpr.orderManager.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;

import com.mpr.orderManager.service.OrderManager;

public class OrderManagerTest {
	OrderManager oManager = new OrderManager();
	
	private final static String STREET = "Brzegi";
	private final static String BUILDING = "55";
	private final static String FLAT = "209";
	private final static String POSTAL = "80-041";
	private final static String CITY = "Gdansk";
	private final static String COUNTRY = "Polska";
	
	@Test
	public void checkConnection(){
		assertNotNull(oManager.getConnection());
	}
}
