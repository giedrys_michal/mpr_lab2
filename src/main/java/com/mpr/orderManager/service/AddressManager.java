package com.mpr.orderManager.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mpr.orderManager.domain.Address;

public class AddressManager {
	private Connection connection;
	private String url = "jdbc:hsqldb:hsql://localhost/work_db";

	private String createTableAddress = "CREATE TABLE Address(id bigint GENERATED BY DEFAULT AS IDENTITY, street varchar(20), buildingNum varchar(10), flatNum varchar(10), postalCode varchar(10), city varchar(50), country varchar(30))";

	private PreparedStatement addAddressStmt;
	private PreparedStatement getAddressStmt;
	private PreparedStatement deleteAllAddressesStmt;
	private PreparedStatement getAllAddressesStmt;
	
	private Statement statement;
	
	public AddressManager() {
		try {
			connection = DriverManager.getConnection(url, "SA", "");
			statement = connection.createStatement();

			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean tableExists = false;
			while (rs.next()) {
				if ("Address".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}

			if (!tableExists)
				statement.executeUpdate(createTableAddress);
			
			addAddressStmt = connection
					.prepareStatement("INSERT INTO Address (street, buildingNum, flatNum, postalCode, city, country) VALUES (?, ?, ?, ?, ?, ?)");
			
			deleteAllAddressesStmt = connection
					.prepareStatement("DELETE FROM Address");
			
			getAllAddressesStmt = connection
					.prepareStatement("SELECT id, street, buildingNum, flatNum, postalCode, city, country FROM Address");


		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public void clearAddresses() {
		try {
			deleteAllAddressesStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int addAddress(Address address) {
		int count = 0;
		try {
			addAddressStmt.setString(1, address.getStreet());
			addAddressStmt.setString(2, address.getBuildingNum());
			addAddressStmt.setString(3, address.getFlatNum());
			addAddressStmt.setString(4, address.getPostalCode());
			addAddressStmt.setString(5, address.getCity());
			addAddressStmt.setString(6, address.getCountry());
			
			count = addAddressStmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return count;
	}
	
	public Address getAddress(long id) {
		Address addr = new Address();
		try {
			getAddressStmt = connection
					.prepareStatement("SELECT id, street, buildingNum, flatNum, postalCode, city, country FROM Address WHERE id=" + id);
			ResultSet rs = getAddressStmt.executeQuery();
			
			while (rs.next()) {				
				addr.setId(rs.getLong("id"));
				addr.setStreet(rs.getString("street"));
				addr.setBuildingNum(rs.getString("buildingNum"));
				addr.setFlatNum(rs.getString("flatNum"));
				addr.setPostalCode(rs.getString("postalCode"));
				addr.setCity(rs.getString("city"));
				addr.setCountry(rs.getString("country"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return addr;
	}
	
	public List<Address> getAllAddresses() {
		List<Address> addresses = new ArrayList<Address>();
		
		try {
			ResultSet rs = getAllAddressesStmt.executeQuery();
			
			while (rs.next()) {
				Address addr = new Address();
				
				addr.setId(rs.getLong("id"));
				addr.setStreet(rs.getString("street"));
				addr.setBuildingNum(rs.getString("buildingNum"));
				addr.setFlatNum(rs.getString("flatNum"));
				addr.setPostalCode(rs.getString("postalCode"));
				addr.setCity(rs.getString("city"));
				addr.setCountry(rs.getString("country"));
				
				addresses.add(addr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return addresses;
	}
}
