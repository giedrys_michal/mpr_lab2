package com.mpr.orderManager.domain;

public class Address {
	private long id;
	private String street;
	private String buildingNum;
	private String flatNum;
	private String postalCode;
	private String city;
	private String country;
	
	public Address() {
	}

	public Address(String street, String buildingNum, String flatNum, String postalCode, String city, String country) {
		super();
		this.street = street;
		this.buildingNum = buildingNum;
		this.flatNum = flatNum;
		this.postalCode = postalCode;
		this.city = city;
		this.country = country;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getBuildingNum() {
		return buildingNum;
	}
	public void setBuildingNum(String buildingNum) {
		this.buildingNum = buildingNum;
	}
	public String getFlatNum() {
		return flatNum;
	}
	public void setFlatNum(String flatNum) {
		this.flatNum = flatNum;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
}
